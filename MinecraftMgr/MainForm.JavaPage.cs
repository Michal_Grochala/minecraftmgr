﻿using MinecraftMgr.DiskOperation;
using MinecraftMgr.TextOperation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MinecraftMgr
{
    partial class MainForm
    {
        private ServerVersionChecker checker = new ServerVersionChecker();

        #region Initialize/Finalize
        protected void ReadJavaSettingsPage(bool refreshServerJarsOnly = false)
        {
            List<string> jarFiles_regular = Directory.EnumerateFiles(".", "*_server*.jar").ToList();
            List<string> jarFiles_spigot = Directory.EnumerateFiles(".", "spigot-*.jar").ToList();
            List<string> jarFiles_bukkit = Directory.EnumerateFiles(".", "craftbukkit-*.jar").ToList();

            List<string> jarFiles = new List<string>();
            jarFiles.AddRange(jarFiles_regular);
            jarFiles.AddRange(jarFiles_spigot);
            jarFiles.AddRange(jarFiles_bukkit);

            jarFiles.Sort(ServerVersionChecker.CompareServerStrings);

            List<string> formattedJars = new List<string>();
            foreach (string jarFile in jarFiles)
                formattedJars.Add(Path.GetFileName(jarFile));

            comboBoxServerName.Items.Clear();
            comboBoxServerName.Items.AddRange(formattedJars.ToArray());

            JavaSettings settings = registry.ReadJavaSettings();

            if (!refreshServerJarsOnly)
            {
                if (comboBoxJavaExe.Items.Contains(settings.JavaExeType))
                    comboBoxJavaExe.SelectedItem = settings.JavaExeType;
                else
                    comboBoxJavaExe.SelectedIndex = 0;

                this.textBoxXms.Text = settings.Xms;
                this.textBoxXmx.Text = settings.Xmx;
                this.textBoxOtherParms.Text = settings.OtherParameters;
                this.textBoxOtherParms.ScrollToCaret();
            }

            if (comboBoxServerName.Items.Contains(settings.ServerJar))
                comboBoxServerName.SelectedItem = settings.ServerJar;
            else if (0 < comboBoxServerName.Items.Count)
                comboBoxServerName.SelectedIndex = 0;
        }
        #endregion

        #region Events
        private void buttonJavaSave_Click(object sender, EventArgs e)
        {
            JavaSettings settings = new JavaSettings();
            settings.JavaExeType = comboBoxJavaExe.Text;
            settings.OtherParameters = textBoxOtherParms.Text.Trim();
            settings.ServerJar = comboBoxServerName.Text;
            settings.Xms = textBoxXms.Text.Trim();
            settings.Xmx = textBoxXmx.Text.Trim();

            registry.WriteJavaSettings(settings);
        }

        private void buttonJavaRefresh_Click(object sender, EventArgs e)
        {
            ReadJavaSettingsPage();
        }

        private void buttonServerRefresh_Click(object sender, EventArgs e)
        {
            ServerPair serverPair = new ServerPair();

            WaitDialog wait = new WaitDialog((ss, ee) =>
            {
                checker.CompareServerJars(serverPair);
            });
            wait.ShowDialog(this);

            if (null != serverPair)
            {
                if (!string.IsNullOrEmpty(serverPair.error))
                {
                    MessageBox.Show(this,
                        "Nie mogę połączyć się ze stroną Minecraft.",
                        "MineCraft Serwer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (string.IsNullOrEmpty(serverPair.server))
                {
                    MessageBox.Show(this,
                        "Masz najnowszą wersję serwera Minecraft.\nNie potrzeba jej aktualizować.",
                        "MineCraft Serwer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    DialogResult result = MessageBox.Show(this,
                        "Jest nowsza wersja serwera: " + serverPair.server + ".\nCzy chcesz pobrać nowszą wersję?\nPobieranie może chwilę potrwać.",
                        "MineCraft Serwer", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                    if (DialogResult.OK == result)
                    {
                        string status = string.Empty;

                        wait = new WaitDialog((ss, ee) =>
                        {
                            BackgroundWorker worker = ss as BackgroundWorker;
                            DoWorkEventArgs args = ee as DoWorkEventArgs;

                            if (null != worker && null != args)
                            {
                                BufferedStream stream = null;
                                FileStream file = null;

                                try
                                {
                                    WebRequest request = FileWebRequest.Create(serverPair.path);
                                    request.Proxy = null;

                                    WebResponse response = request.GetResponse();
                                    long length = response.ContentLength;
                                    long remains = length;
                                    long received = 0;

                                    worker.ReportProgress(0, "Pobieram ...");
                                    if (worker.CancellationPending)
                                    {
                                        status = WaitDialog.CANCELLED;
                                        return;
                                    }

                                    stream = new BufferedStream(response.GetResponseStream());
                                    file = new FileStream(serverPair.server + ".tmp", FileMode.Create, FileAccess.ReadWrite);

                                    byte[] bytes = new byte[1024];
                                    while (0 < remains)
                                    {
                                        int n = stream.Read(bytes, 0, bytes.Length);

                                        if (0 == n)
                                            break;

                                        remains -= n;
                                        received += n;

                                        file.Write(bytes, 0, n);

                                        worker.ReportProgress(Convert.ToInt32(((decimal)received / (decimal)length) * 100));
                                        if (worker.CancellationPending)
                                        {
                                            status = WaitDialog.CANCELLED;
                                            file.Close();
                                            try
                                            {
                                                if (File.Exists(serverPair.server + ".tmp"))
                                                {
                                                    File.Delete(serverPair.server + ".tmp");
                                                }
                                            }
                                            catch (Exception)
                                            {
                                                //do nothing, user will have to delete the file manually or it will be overridden next time
                                            }
                                            return;
                                        }
                                    }

                                    status = WaitDialog.SUCCESS;
                                }
                                catch (Exception)
                                {
                                    file.Close();
                                    try
                                    {
                                        if (File.Exists(serverPair.server + ".tmp"))
                                        {
                                            File.Delete(serverPair.server + ".tmp");
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //do nothing, user will have to delete file manually or it will be overridden next time
                                    }
                                    status = WaitDialog.ERROR;
                                }
                                finally
                                {
                                    stream.Close();
                                    file.Close();
                                }

                                if (WaitDialog.SUCCESS == status)
                                {
                                    try
                                    {
                                        worker.ReportProgress(100, "Kończę");
                                        if (File.Exists(serverPair.server + ".tmp"))
                                        {
                                            File.Move(serverPair.server + ".tmp", serverPair.server);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        // ?
                                    }
                                }
                            }
                        }, true);
                        wait.ShowDialog(this);

                        if (WaitDialog.ERROR == status)
                        {
                            MessageBox.Show(this,
                                "Nie udało się pobrać najnowszej wersji serwera Minecraft.",
                                "MineCraft Serwer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else if (WaitDialog.SUCCESS == status)
                        {
                            ReadJavaSettingsPage(true);

                            MessageBox.Show(this,
                                "Pobieranie zakończone sukcesem.\nTeraz możesz się cieszyć nową wersją serwera Minecraft.",
                                "MineCraft Serwer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
