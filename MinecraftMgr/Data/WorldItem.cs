﻿using MinecraftMgr.DiskOperation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftMgr.Data
{
    public class WorldItem : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public WorldItem(string name, DateTime created)
        {
            Name = name;
            Created = created;
        }

        public string Name { get; set; }
        public DateTime Created { get; set; }
        public bool Favourite { get; set; }

        private Image image;
        public Image Image
        {
            get
            {
                return image;
            }
            set
            {
                if(image != value)
                {
                    image = value;

                    if (null != PropertyChanged)
                        PropertyChanged(this, new PropertyChangedEventArgs("Image"));
                }
            }
        }

        private bool selected;
        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                if (selected != value)
                {
                    selected = value;

                    if (null != PropertyChanged)
                        PropertyChanged(this, new PropertyChangedEventArgs("Selected"));
                }
            }
        }

        public static void PrioritizeWorlds(BindingList<WorldItem> list, RegistrySettings registry)
        {
            if(null != list)
            {
                Dictionary<string, WorldItem> currDirMap = new Dictionary<string, WorldItem>();
                List<string> currDirList = new List<string>();
                List<WorldItem> items = new List<WorldItem>();

                list.ToList().ForEach(x => currDirMap.Add(x.Name, x));
                list.ToList().ForEach(x => currDirList.Add(x.Name));

                LatestDirSettings settings = registry.ReadLatestDirSettings();
                List<string> prevDirList = settings.DirectoryList;

                List<string> newDirList = currDirList.Except(prevDirList).ToList();

                if(0 < newDirList.Count)
                {
                    newDirList.Sort((x,y) => -(currDirMap[x].Created.CompareTo(currDirMap[y].Created)));

                    foreach(string str in newDirList)
                    {
                        WorldItem it = currDirMap[str];
                        it.Created = DateTime.Now;
                        items.Add(it);
                    }
                }

                foreach(Dir dir in settings.LatestDirs)
                {
                    if (!string.IsNullOrEmpty(dir.Name) && currDirMap.ContainsKey(dir.Name))
                    {
                        WorldItem it = currDirMap[dir.Name];
                        it.Created = (DateTime) dir.Creation;
                        items.Add(it);
                    }
                }

                if(items.Count < LatestDirSettings.LAST_DIR_COUNT)
                {
                    List<string> tmp = items.Select(x => x.Name).ToList();
                    List<string> fillGapDirList = currDirList.Except(tmp).ToList();

                    if (0 < fillGapDirList.Count)
                    {
                        fillGapDirList.Sort((x, y) => -(currDirMap[x].Created.CompareTo(currDirMap[y].Created)));

                        for (int i = 0; i < LatestDirSettings.LAST_DIR_COUNT - items.Count; i++)
                        {
                            if (i < fillGapDirList.Count && currDirMap.ContainsKey(fillGapDirList[i]))
                            {
                                items.Add(currDirMap[fillGapDirList[i]]);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }

                list.ToList().ForEach(x => x.Image = global::MinecraftMgr.Properties.Resources.Empty);

                for (int i = 0; i < items.Count; i++)
                {
                    if (0 == i)
                    {
                        if (items[i].Created.Date == DateTime.Today)
                            items[i].Image = global::MinecraftMgr.Properties.Resources.Latest;
                        else
                            items[i].Image = global::MinecraftMgr.Properties.Resources.LatestHalf;
                    }
                    else if (1 == i || 2 == i)
                    {
                        if (items[i].Created.Date == DateTime.Today)
                            items[i].Image = global::MinecraftMgr.Properties.Resources.LatestL;
                        else
                            items[i].Image = global::MinecraftMgr.Properties.Resources.LatestLHalf;
                    }
                    else if (3 == i || 4 == i || 5 == i)
                    {
                        if (items[i].Created.Date == DateTime.Today)
                            items[i].Image = global::MinecraftMgr.Properties.Resources.LatestLL;
                        else
                            items[i].Image = global::MinecraftMgr.Properties.Resources.LatestLLHalf;
                    }
                    else
                    {
                        items[i].Image = global::MinecraftMgr.Properties.Resources.Empty;
                    }
                }

                //save settings
                settings = new LatestDirSettings(currDirList, items.Select<WorldItem, Dir>(x => new Dir(x.Name, x.Created)).ToArray());
                registry.WriteLatestDirSettings(settings);
            }
        }
    }
}
