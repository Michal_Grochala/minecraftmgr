﻿using MinecraftMgr.TextOperation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftMgr
{
    partial class MainForm
    {
        private OutputParsing parsing = new OutputParsing();

        #region Events
        private void minecraftProcessStart(object sender, DoWorkEventArgs e)
        {
            string javaExeType = string.Empty;
            string arguments = string.Empty;
            string serverName = string.Empty;

            this.Invoke((Action)delegate
            {
                javaExeType = comboBoxJavaExe.Text;
                serverName = comboBoxServerName.Text;
                arguments =
                    (!string.IsNullOrEmpty(textBoxXms.Text.Trim()) ? " -Xms" + textBoxXms.Text.Trim() : string.Empty) +
                    (!string.IsNullOrEmpty(textBoxXmx.Text.Trim()) ? " -Xmx" + textBoxXmx.Text.Trim() : string.Empty) +
                    (!string.IsNullOrEmpty(textBoxOtherParms.Text.Trim()) ? " " + textBoxOtherParms.Text.Replace('\r', ' ').Replace('\n', ' ').Trim() : string.Empty) +
                    " -jar " + comboBoxServerName.Text +
                    " nogui";
            });

            if (!string.IsNullOrEmpty(serverName))
            {
                minecraftProcess.StartInfo.FileName = javaExeType;
                minecraftProcess.StartInfo.Arguments = arguments.Trim();

                minecraftProcess.Start();

                streamWriter = minecraftProcess.StandardInput;
                minecraftProcess.BeginErrorReadLine();
                minecraftProcess.BeginOutputReadLine();
            }
        }

        private void minecraftProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            this.Invoke((Action)delegate
            {
                RichTextBoxTextAppender.AppendTextToLog(richTextBoxLogs, e.Data + OutputParsing.END_OF_LINE, Color.DarkRed);
                richTextBoxLogs.ScrollToCaret();
            });
        }

        private void minecraftProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            this.Invoke((Action)delegate
            {
                List<Format> formats = new List<Format>();
                formats.Add(new Format(OutputParsing.EULA_WARN, SystemColors.ControlText, false, false, true));
                formats.Add(new Format(OutputParsing.TRACE_WARN, Color.Peru));
                formats.Add(new Format(OutputParsing.WORLD_NAME, SystemColors.ControlText, true));
                formats.Add(new Format(OutputParsing.COMMAND_BLOCK, true));

                bool isCommandBlock = false;
                List<FormatedOutput> output = parsing.GetFormatedOutput(e.Data, formats);

                foreach (FormatedOutput fout in output)
                {
                    Format format = fout.format;

                    if (format.isCommandBlock && checkBoxSkipCommandBlocks.Checked)
                    {
                        isCommandBlock = true;
                        continue;
                    }
                    else
                    {
                        isCommandBlock = false;
                    }

                    RichTextBoxTextAppender.AppendTextToLog(richTextBoxLogs, fout.text, format.color, format.bold, format.italic, format.underline);
                }

                if (!isCommandBlock || !checkBoxSkipCommandBlocks.Checked)
                {
                    RichTextBoxTextAppender.AppendTextToLog(richTextBoxLogs, OutputParsing.END_OF_LINE, SystemColors.ControlText);
                    richTextBoxLogs.ScrollToCaret();
                }

                int perc = parsing.GetProgressPercent(e.Data);
                progressBar.Value = (progressBar.Value < perc ? perc : progressBar.Value);
            });
        }

        private void minecraftProcess_Exited(object sender, EventArgs e)
        {
            minecraftProcess.CancelOutputRead();
            minecraftProcess.CancelErrorRead();
            minecraftProcess.Close();

            this.Invoke((Action)delegate
            {
                SetServerStopped();
            });
        }
        #endregion
    }
}
