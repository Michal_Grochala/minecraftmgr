﻿using MinecraftMgr.Data;
using MinecraftMgr.DiskOperation;
using MinecraftMgr.TextOperation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MinecraftMgr
{
    public partial class MainForm : Form
    {
        private RegistrySettings registry = new RegistrySettings();
        private PropertiesFile properties = null;
        private StreamWriter streamWriter = null;
        private BindingList<WorldItem> worldList = new BindingList<WorldItem>();
        private SortedList<string, string> sortedWorlds = new SortedList<string, string>();

        public MainForm()
        {
            InitializeComponent();

            if(!DesignMode)
            {
                properties = new PropertiesFile("server.properties");

                bindingSourceCombo.DataSource = worldList;
                bindingSourceGrid.DataSource = worldList;

                BuildWorldList();
                InitializeControls();
            }

            tabControl.SelectedTab = tabPageRemoveWorld;
            IsMinecraftServerRunning = false;
        }

        #region Properties
        protected bool IsMinecraftServerRunning { get; set; }
        #endregion

        #region Initialize/Finalize
        protected void InitializeControls()
        {
            //server.properties
            string world = properties.get(comboBoxName.Tag, "World");
            WorldItem item = worldList.ToList().Find(w => w.Name == world);

            if (null != item)
            {
                item.Selected = true;
                int index = worldList.IndexOf(item);
                comboBoxName.SelectedIndex = (0 <= index ? index : 0);
            }
            else if( 0 < worldList.Count())
            {
                item = worldList.First();
                item.Selected = true;
                comboBoxName.SelectedIndex = 0;
            }

            this.comboBoxGamemode.SelectedIndex = properties.getInt(comboBoxGamemode.Tag, 0);
            this.comboBoxDifficulty.SelectedIndex = properties.getInt(comboBoxDifficulty.Tag, 1);
            this.comboBoxLevelType.Text = properties.get(comboBoxLevelType.Tag, "DEFAULT");

            this.checkBoxAllowCommandBlock.Checked = properties.getBool(checkBoxAllowCommandBlock.Tag, true);
            this.checkBoxAllowFly.Checked = properties.getBool(checkBoxAllowFly.Tag, false);
            this.checkBoxAllowNether.Checked = properties.getBool(checkBoxAllowNether.Tag, true);
            this.checkBoxAnnounceAchivements.Checked = properties.getBool(checkBoxAnnounceAchivements.Tag, true);
            this.checkBoxForceGamemode.Checked = properties.getBool(checkBoxForceGamemode.Tag, true);
            this.checkBoxGenerateStruct.Checked = properties.getBool(checkBoxGenerateStruct.Tag, true);
            this.checkBoxPvp.Checked = properties.getBool(checkBoxPvp.Tag, true);
            this.checkBoxSpawnAnimals.Checked = properties.getBool(checkBoxSpawnAnimals.Tag, true);
            this.checkBoxSpawnMonsters.Checked = properties.getBool(checkBoxSpawnMonsters.Tag, true);
            this.checkBoxSpawnNpcs.Checked = properties.getBool(checkBoxSpawnNpcs.Tag, true);

            this.checkBoxOnlineMode.Checked = properties.getBool(checkBoxOnlineMode.Tag, false);
            this.textBoxUserNumber.Text = properties.get(textBoxUserNumber.Tag, "20");

            //Java settings
            ReadJavaSettingsPage();
        }

        protected void FinalizeControls()
        {
            properties.set(comboBoxName.Tag, comboBoxName.Text);
            properties.set("level-seed", comboBoxName.Text);
            properties.set(comboBoxGamemode.Tag, comboBoxGamemode.SelectedIndex);
            properties.set(comboBoxDifficulty.Tag, comboBoxDifficulty.SelectedIndex);
            properties.set(comboBoxLevelType.Tag, comboBoxLevelType.Text);

            properties.set(checkBoxAllowCommandBlock.Tag, (checkBoxAllowCommandBlock.Checked ? "true" : "false"));
            properties.set(checkBoxAllowFly.Tag, (checkBoxAllowFly.Checked ? "true" : "false"));
            properties.set(checkBoxAllowNether.Tag, (checkBoxAllowNether.Checked ? "true" : "false"));
            properties.set(checkBoxAnnounceAchivements.Tag, (checkBoxAnnounceAchivements.Checked ? "true" : "false"));
            properties.set(checkBoxForceGamemode.Tag, (checkBoxForceGamemode.Checked ? "true" : "false"));
            properties.set(checkBoxGenerateStruct.Tag, (checkBoxGenerateStruct.Checked ? "true" : "false"));
            properties.set(checkBoxOnlineMode.Tag, (checkBoxOnlineMode.Checked ? "true" : "false"));
            properties.set(checkBoxPvp.Tag, (checkBoxPvp.Checked ? "true" : "false"));
            properties.set(checkBoxSpawnAnimals.Tag, (checkBoxSpawnAnimals.Checked ? "true" : "false"));
            properties.set(checkBoxSpawnMonsters.Tag, (checkBoxSpawnMonsters.Checked ? "true" : "false"));
            properties.set(checkBoxSpawnNpcs.Tag, (checkBoxSpawnNpcs.Checked ? "true" : "false"));

            properties.set(checkBoxOnlineMode.Tag, (checkBoxOnlineMode.Checked ? "true" : "false"));
            properties.set(textBoxUserNumber.Tag, textBoxUserNumber.Text);

            properties.Save();
        }
        #endregion

        #region Events
        protected override void OnClosing(CancelEventArgs e)
        {
            if (this.buttonStart.Text == "Stop")
            {
                MessageBox.Show(this, "Najpierw musisz wyłączyć serwer.", "MineCraft Serwer", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
            else
            {
                base.OnClosing(e);
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(buttonStart.Tag) == "START")
            {
                if (!string.IsNullOrEmpty(comboBoxServerName.Text))
                {
                    errorProvider.SetError(comboBoxServerName, string.Empty);

                    if (ValidateCtrls(true) && !string.IsNullOrEmpty(comboBoxName.Text))
                    {
                        progressBar.Value = 5;
                        buttonStart.Text = "Startuję";

                        try
                        {
                            FinalizeControls();
                            AddNewWorldToList();

                            HideDeleteButton(true);
                            backgroundWorker.RunWorkerAsync();

                            SetControlsState(false);
                            buttonStart.Text = "Stop";
                            buttonStart.Tag = "STOP";
                            IsMinecraftServerRunning = true;
                        }
                        catch(Exception)
                        {
                            SetServerStopped();
                        }
                    }
                }
                else
                {
                    tabControl.SelectedTab = tabPageJava;
                    errorProvider.SetError(comboBoxServerName, "Wybierz wersję serwera.");

                    MessageBox.Show(this, "Nie można włączyć serwera, ponieważ brakuje pliku 'minecraft_server(*).jar'.",
                        "MineCraft Serwer",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (null != streamWriter)
                    streamWriter.WriteLineAsync("stop now");

                try
                {
                    Process p = Process.GetProcessById(minecraftProcess.Id);

                    if (null != p)
                        p.Kill();
                }
                catch (Exception)
                {
                    //Nothing to do, process does not exist
                }
            }
        }

        private void comboBoxName_Leave(object sender, EventArgs e)
        {
            ValidateCtrls(false);
        }

        private void textBoxCommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (!textBoxCommandLine.Text.Trim().ToLower().StartsWith("stop") 
                    && !textBoxCommandLine.Text.Trim().ToLower().StartsWith("/stop")
                    && !string.IsNullOrEmpty(textBoxCommandLine.Text.Trim()))
                {
                    if (null != streamWriter)
                    {
                        streamWriter.WriteLineAsync(textBoxCommandLine.Text);
                        RichTextBoxTextAppender.AppendTextToLog(richTextBoxLogs, textBoxCommandLine.Text.Trim() + "\r\n", SystemColors.ControlText, false, true);
                    }
                }

                textBoxCommandLine.Text = string.Empty;
            }
        }

        private void buttonServerCommand_Click(object sender, EventArgs e)
        {
            textBoxCommandLine_KeyDown(sender, new KeyEventArgs(Keys.Enter));
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            WaitDialog wati = new WaitDialog((ss, ee) =>
            {
                this.Invoke((Action)delegate
                {
                    WorldItem item = comboBoxName.SelectedItem as WorldItem;
                    int selectedIndex = bindingSourceGrid.Position;

                    BuildWorldList();

                    if (null != item)
                    {
                        WorldItem itm = worldList.ToList().Find(w => w.Name == item.Name);
                        if (null != itm)
                        {
                            itm.Selected = true;
                            comboBoxName.SelectedItem = itm;
                        }
                        else if (null != comboBoxName.SelectedItem)
                        {
                            WorldItem itt = comboBoxName.SelectedItem as WorldItem;
                            if (null != itt)
                            {
                                itt.Selected = true;
                            }
                        }
                    }

                    if (0 < selectedIndex)
                        if (0 < bindingSourceGrid.List.Count && selectedIndex < bindingSourceGrid.List.Count)
                            bindingSourceGrid.Position = selectedIndex;
                });
            });
            wati.ShowDialog(this);

            /*WorldItem item = comboBoxName.SelectedItem as WorldItem;
            int selectedIndex = bindingSourceGrid.Position;

            BuildWorldList();

            if (null != item)
            {
                WorldItem itm = worldList.ToList().Find(w => w.Name == item.Name);
                if (null != itm)
                {
                    itm.Selected = true;
                    comboBoxName.SelectedItem = itm;
                }
                else if (null != comboBoxName.SelectedItem)
                {
                    WorldItem itt = comboBoxName.SelectedItem as WorldItem;
                    if(null != itt)
                    {
                        itt.Selected = true;
                    }
                }
            }

            if (0 < selectedIndex)
                if (0 < bindingSourceGrid.List.Count && selectedIndex < bindingSourceGrid.List.Count)
                    bindingSourceGrid.Position = selectedIndex;*/
        }

        private void toolStripMenuItemClear_Click(object sender, EventArgs e)
        {
            richTextBoxLogs.Text = string.Empty;
        }

        private void comboBoxName_SelectedIndexChanged(object sender, EventArgs e)
        {
            WorldItem item = comboBoxName.SelectedItem as WorldItem;

            if(null != item)
            {
                SuspendBindingSourceEvents();

                worldList.ToList().ForEach(w => w.Selected = false);
                item.Selected = true;

                ResumeBindSourceEvents();

                bindingSourceGrid.Position = worldList.IndexOf(item);
            }
        }

        private void textBoxUserNumber_Validated(object sender, EventArgs e)
        {
            textBoxUserNumber_KeyUp(sender, null);
        }

        private void textBoxUserNumber_KeyUp(object sender, KeyEventArgs e)
        {
            string userNum = textBoxUserNumber.Text;
            int num = 0;

            if (!Int32.TryParse(userNum, out num) || 100 < num)
            {
                textBoxUserNumber.Text = "20";
                textBoxUserNumber.Select(0, 2);
            }
        }
        #endregion

        private bool ValidateCtrls(bool showMessageBox)
        {
            StringBuilder result = new StringBuilder();

            errorProvider.SetError(comboBoxName, string.Empty);

            char[] invalidFilenameChars = System.IO.Path.GetInvalidFileNameChars();
            Array.Sort(invalidFilenameChars);

            foreach (var characterToTest in comboBoxName.Text)
            {
                if (Array.BinarySearch(invalidFilenameChars, characterToTest) >= 0)
                {
                    result.Append(characterToTest);
                }
            }

            if (0 < result.Length)
            {
                errorProvider.SetError(comboBoxName, "Nieprawidłowe znaki w nazwie: " + result);

                if (showMessageBox)
                    MessageBox.Show(this, "Nie można włączyć serwera, ponieważ użyłeś nieprawidłowych znaków w nazwie.\nNieprawidłowe znaki to: " + result, "MineCraft Serwer",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                return false;
            }

            return true;
        }

        private void SetControlsState(bool enabled)
        {
            this.comboBoxName.Enabled = enabled;
            this.buttonRefresh.Enabled = enabled;

            //Settings tab
            this.groupBox1.Enabled = enabled;
            this.groupBox2.Enabled = enabled;

            //Command tab
            this.textBoxCommandLine.Enabled = !enabled;
            this.buttonServerCommand.Enabled = !enabled;
            this.columnSelected.ReadOnly = !enabled;

            //Java tab
            this.comboBoxJavaExe.Enabled = enabled;
            this.textBoxXms.Enabled = enabled;
            this.textBoxXmx.Enabled = enabled;
            this.buttonJavaRefresh.Enabled = enabled;
            this.buttonJavaSave.Enabled = enabled;
            this.textBoxOtherParms.Enabled = enabled;
            this.comboBoxServerName.Enabled = enabled;
            this.buttonServerRefresh.Enabled = enabled;

            //Bukkit filter tab
            this.checkBoxActivateBukkit.Enabled = enabled;
            this.groupBoxSuffixes.Enabled = enabled && checkBoxActivateBukkit.Checked;
            this.buttonBukkitRefresh.Enabled = enabled;
            this.buttonBukkitSave.Enabled = enabled;
        }

        private void AddNewWorldToList()
        {
            string worldName = comboBoxName.Text;

            if (worldList.Count(w => w.Name == worldName) <= 0)
            {
                sortedWorlds.Add(worldName, worldName);
                WorldItem item = new WorldItem(worldName, DateTime.Now);
                worldList.Insert(sortedWorlds.IndexOfKey(worldName), item);

                SuspendBindingSourceEvents();

                worldList.ToList().ForEach(w => w.Selected = false);
                WorldItem.PrioritizeWorlds(worldList, registry);

                ResumeBindSourceEvents();

                comboBoxName.SelectedIndex = worldList.IndexOf(item);
            }
        }

        private void BuildWorldList()
        {
            ReadBukkitFilterSettings();

            DirectoryInfo dirInfo = new DirectoryInfo(".");
            List<DirectoryInfo> infos = dirInfo.GetDirectories().ToList();

            infos.Sort((x, y) => x.Name.CompareTo(y.Name));

            SuspendBindingSourceEvents();

            worldList.Clear();
            sortedWorlds.Clear();

            foreach (DirectoryInfo directory in infos)
            {
                if (File.Exists(directory.Name + Path.DirectorySeparatorChar + "session.lock")) //level.dat
                {
                    if(bukkitFilterSettings.isActive)
                    {
                        if (directory.Name.EndsWith(bukkitFilterSettings.NetherSuffix) || directory.Name.EndsWith(bukkitFilterSettings.EndSuffix))
                            continue;
                    }

                    WorldItem item = new WorldItem(directory.Name, directory.CreationTime);
                    worldList.Add(item);
                    sortedWorlds.Add(item.Name, item.Name);

                    if(File.Exists(directory.Name + Path.DirectorySeparatorChar + "I.Like.It"))
                    {
                        item.Favourite = true;
                    }
                }
            }

            WorldItem.PrioritizeWorlds(worldList, registry);

            ResumeBindSourceEvents(true);
        }

        private void SetServerStopped()
        {
            buttonStart.Text = "Start";
            buttonStart.Tag = "START";
            IsMinecraftServerRunning = false;

            SetControlsState(true);
            HideDeleteButton(false);

            progressBar.Value = 0;
        }

        #region Binding Source and Layout Operations
        private void SuspendBindingSourceEvents()
        {
            comboBoxName.BeginUpdate();

            bindingSourceCombo.RaiseListChangedEvents = false;
            bindingSourceGrid.RaiseListChangedEvents = false;
        }

        private void ResumeBindSourceEvents(bool resetBinidings = false)
        {
            bindingSourceCombo.RaiseListChangedEvents = true;
            bindingSourceGrid.RaiseListChangedEvents = true;

            if (resetBinidings)
            {
                bindingSourceCombo.ResetBindings(false);
                bindingSourceGrid.ResetBindings(false);
            }

            comboBoxName.EndUpdate();
        }
        #endregion
    }
}
