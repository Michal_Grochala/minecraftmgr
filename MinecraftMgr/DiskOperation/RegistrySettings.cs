﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftMgr.DiskOperation
{
    public class RegistrySettings
    {
        protected const string REGISTRY_PATH = @"SOFTWARE\Michal Grochala\MinecraftMgr";
        protected const char SEPARATOR = '|';
        protected char[] SEPARATORS = new char[] { '|' };

        protected RegistryKey OpenRegistry(string registryPath)
        {
            RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Default);
            return key.CreateSubKey(registryPath, RegistryKeyPermissionCheck.ReadWriteSubTree);
        }

        #region Java Settings
        public void WriteJavaSettings(JavaSettings settings)
        {
            RegistryKey subKey = OpenRegistry(REGISTRY_PATH);

            if (null != subKey)
            {
                subKey.SetValue("JavaExeType", settings.JavaExeType, RegistryValueKind.String);
                subKey.SetValue("Xms", settings.Xms, RegistryValueKind.String);
                subKey.SetValue("Xmx", settings.Xmx, RegistryValueKind.String);
                subKey.SetValue("OtherParameters", settings.OtherParameters, RegistryValueKind.String);
                subKey.SetValue("ServerJar", settings.ServerJar, RegistryValueKind.String);

                subKey.Close();
            }
        }

        public JavaSettings ReadJavaSettings()
        {
            JavaSettings settings = new JavaSettings();
            RegistryKey subKey = OpenRegistry(REGISTRY_PATH);

            if (null != subKey)
            {
                settings.JavaExeType = subKey.GetValue("JavaExeType", "javaw") as string;
                settings.Xms = subKey.GetValue("Xms", "512m") as string;
                settings.Xmx = subKey.GetValue("Xmx", "1024m") as string;
                settings.OtherParameters = subKey.GetValue("OtherParameters", string.Empty) as string;
                settings.ServerJar = subKey.GetValue("ServerJar", string.Empty) as string;

                subKey.Close();
            }

            return settings;
        }
        #endregion

        #region Latest Dir Settings
        public void WriteLatestDirSettings(LatestDirSettings settings)
        {
            RegistryKey subKey = OpenRegistry(REGISTRY_PATH + @"\LatestDirSettings");

            if(null != subKey)
            {
                string directoryList = ConvertListToString(settings.DirectoryList);
                subKey.SetValue("DirectoryList", directoryList, RegistryValueKind.String);

                for(int i=0; i<settings.LatestDirs.Length; i++)
                {
                    subKey.SetValue("LatestDirs.Name." + i, settings.LatestDirs[i].Name, RegistryValueKind.String);
                    subKey.SetValue("LatestDirs.Creation." + i, settings.LatestDirs[i].Creation.ToString(), RegistryValueKind.String);
                }

                subKey.Close();
            }
        }

        public LatestDirSettings ReadLatestDirSettings()
        {
            LatestDirSettings settings = new LatestDirSettings();
            RegistryKey subKey = OpenRegistry(REGISTRY_PATH + @"\LatestDirSettings");

            if (null != subKey)
            {
                settings.DirectoryList = ConvertStringToList(subKey.GetValue("DirectoryList", string.Empty) as string);

                for(int i=0; i<settings.LatestDirs.Length; i++)
                {
                    settings.LatestDirs[i].Name = subKey.GetValue("LatestDirs.Name." + i, string.Empty) as string;

                    string creation = subKey.GetValue("LatestDirs.Creation." + i, null) as string;
                    DateTime creationDateTime;

                    if(DateTime.TryParse(creation, out creationDateTime))
                    {
                        settings.LatestDirs[i].Creation = creationDateTime;
                    }
                    else
                    {
                        settings.LatestDirs[i].Creation = null;
                    }
                }

                subKey.Close();
            }

            return settings;
        }

        private List<string> ConvertStringToList(string line)
        {
            if (string.IsNullOrEmpty(line))
            {
                return new List<string>();
            }
            else
            {
                return line.Split(SEPARATORS, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
        }

        private string ConvertListToString(List<string> list)
        {
            string line = string.Empty;

            if (null != list && 0 < list.Count)
            {
                foreach (string str in list)
                {
                    line += str + SEPARATOR;
                }
            }

            return line;
        }
        #endregion

        #region Bukkit Filter Settings
        public void WriteBukkintFilterSettings(BukkitFilter settings)
        {
            RegistryKey subKey = OpenRegistry(REGISTRY_PATH + @"\BukkitFilter");

            if (null != subKey)
            {
                subKey.SetValue("isActive", (settings.isActive ? "Y" : "N"), RegistryValueKind.String);
                subKey.SetValue("NetherSuffix", settings.NetherSuffix, RegistryValueKind.String);
                subKey.SetValue("EndSuffix", settings.EndSuffix, RegistryValueKind.String);

                subKey.Close();
            }
        }

        public BukkitFilter ReadBukkitFilterSettings()
        {
            BukkitFilter settings = new BukkitFilter();
            RegistryKey subKey = OpenRegistry(REGISTRY_PATH + @"\BukkitFilter");

            if (null != subKey)
            {
                settings.isActive = ("Y" == (subKey.GetValue("isActive", "N") as string));
                settings.NetherSuffix = subKey.GetValue("NetherSuffix", "_nether") as string;
                settings.EndSuffix = subKey.GetValue("EndSuffix", "_the_end") as string;

                subKey.Close();
            }

            return settings;
        }
        #endregion
    }

    public class JavaSettings
    {
        public string JavaExeType;
        public string Xms;
        public string Xmx;
        public string OtherParameters;
        public string ServerJar;

        public JavaSettings()
        {
        }

        public JavaSettings(string javaExeType, string xms, string xmx, string otherParams, string serverJar)
        {
            this.JavaExeType = javaExeType;
            this.Xms = xms;
            this.Xmx = xmx;
            this.OtherParameters = otherParams;
            this.ServerJar = serverJar;
        }
    }

    public class LatestDirSettings
    {
        public static int LAST_DIR_COUNT = 6;
        private Dir[] latestDirs;
        private List<string> directoryList;

        public LatestDirSettings(List<string> directorylist, Dir[] latestDirs)
        {
            this.DirectoryList = directorylist;
            this.LatestDirs = latestDirs;
        }

        public LatestDirSettings()
            : this(null, null)
        {
        }

        public List<string> DirectoryList
        {
            get
            {
                return directoryList;
            }
            set
            {
                if(null != value)
                {
                    directoryList = value;
                }
                else
                {
                    directoryList = new List<string>();
                }
            }
        }

        public Dir[] LatestDirs
        {
            get
            {
                return latestDirs;
            }
            set
            {
                if (null != value && LAST_DIR_COUNT == value.Length)
                {
                    latestDirs = value;
                }
                else if (null == value)
                {
                    latestDirs = new Dir[LAST_DIR_COUNT];
                    for (int i = 0; i < latestDirs.Length; i++)
                        latestDirs[i] = new Dir(string.Empty, null);
                }
                else
                {
                    latestDirs = new Dir[LAST_DIR_COUNT];
                    for (int i = 0; i < latestDirs.Length; i++)
                        latestDirs[i] = new Dir(string.Empty, null);

                    for(int i=0; i<value.Length; i++)
                    {
                        if (i < LAST_DIR_COUNT)
                        {
                            latestDirs[i] = value[i];
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    public class Dir
    {
        public string Name;
        public DateTime? Creation;

        public Dir(string name, DateTime? created)
        {
            this.Name = name;
            this.Creation = created;
        }
    }

    public class BukkitFilter
    {
        public bool isActive;
        public string EndSuffix;
        public string NetherSuffix;
    }
}
