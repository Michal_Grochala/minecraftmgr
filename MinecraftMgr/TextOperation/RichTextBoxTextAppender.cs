﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MinecraftMgr.TextOperation
{
    internal class RichTextBoxTextAppender
    {
        public static void AppendTextToLog(RichTextBox richTextBox, string text, Color color, bool bold = false, bool italic = false, bool underline = false)
        {
            if (null != richTextBox && !string.IsNullOrEmpty(text))
            {
                richTextBox.SelectionStart = richTextBox.Text.Length;
                richTextBox.SelectionLength = 0;
                richTextBox.SelectionColor = color;

                Font rtfFont = richTextBox.SelectionFont;
                FontStyle fontStyle = FontStyle.Regular;

                if (bold)
                    fontStyle |= FontStyle.Bold;
                if (italic)
                    fontStyle |= FontStyle.Italic;
                if (underline)
                    fontStyle |= FontStyle.Underline;


                Font font = new Font(richTextBox.SelectionFont.FontFamily, richTextBox.SelectionFont.Size, fontStyle);
                richTextBox.SelectionFont = font;

                richTextBox.AppendText(text);
                richTextBox.SelectionColor = richTextBox.ForeColor;
                richTextBox.SelectionFont = rtfFont;
            }
        }
    }
}
