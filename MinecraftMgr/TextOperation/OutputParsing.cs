﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MinecraftMgr.TextOperation
{
    public class OutputParsing
    {
        #region Regular Expressions
        public const string WORLD_NAME                   = @"(?<=level\s"").*?(?="")";
        public const string TRACE_WARN                   = @"^\[((:?)\d\d)+.*?WARN\]:\s";
        public const string EULA_WARN                    = @"(?<=\s)eula(\.txt)?(?=\s)";
        public const string COMMAND_BLOCK                = @"^\[((:?)\d\d)+.*?INFO\]:\s((\[@:\s)|(CommandBlock)).*";

        public const string END_OF_LINE                  = "\r\n";
        #endregion

        public List<FormatedOutput> GetFormatedOutput(string text, List<Format> formats)
        {
            List<FormatedOutput> output = new List<FormatedOutput>();

            if (!string.IsNullOrEmpty(text) && null != formats)
            {
                GetFormattedOutput(output, new Input(text, 0), formats);
                output.Sort((x, y) => x.order.CompareTo(y.order));
            }

            return output;
        }

        private void GetFormattedOutput(List<FormatedOutput> output, Input input, List<Format> formats)
        {
            if (null != output)
            {
                bool matched = false;

                foreach (Format format in formats)
                {
                    Regex regex = new Regex(format.regex, RegexOptions.IgnoreCase);
                    Match match = regex.Match(input.text);

                    if (match.Success)
                    {
                        string text = format.isCommandBlock ? input.text : match.Value;

                        FormatedOutput item = new FormatedOutput(text, format);
                        item.order = match.Index;

                        if (0 < match.Index && !format.isCommandBlock)
                        {
                            FormatedOutput it2 = new FormatedOutput(input.text.Substring(0, match.Index));
                            it2.order = 0;
                            output.Add(it2);
                        }

                        if (match.Index + match.Length < input.text.Length && !format.isCommandBlock)
                        {
                            FormatedOutput it2 = new FormatedOutput(input.text.Substring(match.Index + match.Length));
                            it2.order = match.Index + match.Length;
                            output.Add(it2);
                        }

                        matched = true;
                        output.Add(item);
                        break;
                    }
                }

                if (!matched)
                {
                    output.Add(new FormatedOutput(input.text));
                }
            }
        }

        #region PRIVATE CLASS
        private class Input
        {
            public string text;
            public int order;

            public Input()
            {
            }

            public Input(string text, int order)
            {
                this.text = text;
                this.order = order;
            }
        }
        #endregion

        #region PROGRESS
        protected const string PROGRESS_PR = @"(?<=area:\s)[0-9]+(?=%$)";
        protected const string PROGRESS_FINISHED = @"""help""\sor\s""\?""$";

        public int GetProgressPercent(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                Regex regex = new Regex(PROGRESS_FINISHED);
                Match match = regex.Match(text);

                if (match.Success)
                {
                    return 100;
                }
                else
                {
                    regex = new Regex(PROGRESS_PR);
                    match = regex.Match(text);

                    if (match.Success)
                        return Convert.ToInt32(match.Value);
                }
            }

            return 10;
        }
        #endregion
    }

    public class Format
    {
        public Format()
        {
        }

        public Format(string regex, Color color, bool bold = false, bool italic = false, bool underline = false)
        {
            this.regex = regex;
            this.color = color;
            this.bold = bold;
            this.italic = italic;
            this.underline = underline;
        }

        public Format(string regex, bool isCommandBlock)
        {
            this.regex = regex;
            this.isCommandBlock = isCommandBlock;
        }

        public string regex;
        public Color color = SystemColors.ControlText;
        public bool bold = false;
        public bool italic = false;
        public bool underline = false;
        public bool isCommandBlock = false;
    }

    public class FormatedOutput
    {
        private static Format defaultFormat = new Format();

        public FormatedOutput(string text)
        {
            this.text = text;
            this.format = defaultFormat;
        }

        public FormatedOutput(string text, Format format)
        {
            this.text = text;
            this.format = format;
        }

        public string text;
        public Format format;
        public int order;
    }
}
