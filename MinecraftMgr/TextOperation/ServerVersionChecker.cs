﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MinecraftMgr.TextOperation
{
    public class ServerVersionChecker
    {
        protected const string downloadUrl = @"https://minecraft.net/pl/download/server";
        protected const string httpsJarPath = @"https://[a-z0-9\/\.]+/minecraft_server[0-9.]+jar";
        protected const string serverJar = @"(?<=/)minecraft_server.*jar";

        public ServerVersionChecker()
        {
        }

        public void CompareServerJars(ServerPair serverPair)
        {
            if (null != serverPair)
            {
                GetAvailableVersionPath(serverPair);

                if (string.IsNullOrEmpty(serverPair.error) && !string.IsNullOrEmpty(serverPair.path))
                {
                    string serverJar = GetAvailableServerString(serverPair.path);
                    List<string> jarFiles = new List<string>();

                    foreach (string str in Directory.EnumerateFiles(".", "minecraft_server*.jar"))
                    {
                        jarFiles.Add(Path.GetFileName(str));
                    }

                    jarFiles.Sort(ServerVersionChecker.CompareServerStrings);

                    if (!string.IsNullOrEmpty(serverJar) && 0 < jarFiles.Count)
                    {
                        int res = ServerVersionChecker.CompareServerStrings(serverJar, jarFiles.First());

                        if (res < 0)
                        {
                            serverPair.server = serverJar;
                        }
                        else
                        {
                            //no newer version available
                            serverPair.server = string.Empty;
                        }
                    }
                    else if(!string.IsNullOrEmpty(serverJar))
                    {
                        serverPair.server = serverJar;
                    }
                }
            }
        }

        protected string GetAvailableServerString(string avialbleServerPath)
        {
            Regex regex = new Regex(serverJar);
            Match match = regex.Match(avialbleServerPath);

            if (match.Success)
                return match.Value;

            return string.Empty;
        }

        protected void GetAvailableVersionPath(ServerPair serverPair)
        {
            if (null != serverPair)
            {
                StringBuilder builder = new StringBuilder();
                StreamReader reader = null;

                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                try
                {
                    WebRequest request = HttpWebRequest.Create(new Uri(downloadUrl));
                    request.Proxy = null;

                    WebResponse response = request.GetResponse();
                    reader = new StreamReader(response.GetResponseStream());

                    string line = string.Empty;
                    while (null != line)
                    {
                        line = reader.ReadLine();

                        if (!string.IsNullOrEmpty(line))
                            builder.Append(line);
                    }

                    if (0 < builder.Length)
                    {
                        Regex regex = new Regex(httpsJarPath, RegexOptions.IgnoreCase);
                        Match match = regex.Match(builder.ToString());

                        if (match.Success)
                        {
                            serverPair.path = match.Value;
                        }
                    }
                }
                catch (Exception e)
                {
                    serverPair.error = e.Message;
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        public static int CompareServerStrings(string x, string y)
        {
            x = (x.EndsWith(".jar") ? x.Substring(0, x.Length - 4) : x);
            y = (y.EndsWith(".jar") ? y.Substring(0, y.Length - 4) : y);

            string[] Xarr = x.Split(new string[] { ".", " ", "-" }, StringSplitOptions.RemoveEmptyEntries);
            string[] Yarr = y.Split(new string[] { ".", " ", "-" }, StringSplitOptions.RemoveEmptyEntries);

            int i = 0;
            for (i = 0; i < Xarr.Length; i++)
            {
                if (i < Yarr.Length)
                {
                    string iSx = Xarr[i];
                    string iSy = Yarr[i];

                    int iIx, iIy;
                    if (Int32.TryParse(iSx, out iIx) && Int32.TryParse(iSy, out iIy))
                    {
                        int res = iIx.CompareTo(iIy);

                        if (0 != res)
                            return res * (-1);
                    }
                    else
                    {
                        int res = iSx.CompareTo(iSy);

                        if (0 != res)
                            return res * (-1);
                    }
                }
                else
                {
                    return 1 * (-1); //x is grater than y
                }
            }

            return -1 * (-1);
        }
    }

    public class ServerPair
    {
        public string server;
        public string path;
        public string error;
    }
}
