﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MinecraftMgr
{
    public partial class WaitDialog : Form
    {
        public const string SUCCESS = "SUCCESS";
        public const string CANCELLED = "CANCELLED";
        public const string ERROR = "ERROR";

        private object argument = null;
        private object result = null;

        public WaitDialog(DoWorkEventHandler work, object argument, bool allowCancel = false)
        {
            InitializeComponent();

            this.argument = argument;
            buttonCancel.Enabled = allowCancel;

            if (!DesignMode)
            {
                this.backgroundWorker.DoWork += work;
            }
        }

        public WaitDialog(DoWorkEventHandler work, bool allowCancel = false)
            : this(work, null, allowCancel)
        {
        }

        #region Overrides
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.backgroundWorker.RunWorkerAsync(argument);
        }

        new public object ShowDialog(IWin32Window owner)
        {
            base.ShowDialog(owner);
            return result;
        }
        #endregion

        #region Events

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            result = e.Result;

            this.Invoke((Action)delegate
            {
                this.Close();
            });
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (progressBarWait.Style == ProgressBarStyle.Marquee)
                progressBarWait.Style = ProgressBarStyle.Continuous;

            progressBarWait.Value = e.ProgressPercentage;

            if (!string.IsNullOrEmpty(Convert.ToString(e.UserState)))
                labelWait.Text = e.UserState.ToString();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            buttonCancel.Enabled = false;
            labelWait.Text = "Anulowanie ...";
            backgroundWorker.CancelAsync();
        }
        #endregion
    }
}
