﻿using Microsoft.VisualBasic.FileIO;
using MinecraftMgr.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MinecraftMgr
{
    partial class MainForm
    {
        protected const string ADD = "ADD";
        protected const string DEL = "DEL";

        #region Events
        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView gridView = sender as DataGridView;

            if (null != gridView && 0 <= e.ColumnIndex && 0 <= e.RowIndex)
            {
                WorldItem item = gridView.Rows[e.RowIndex].DataBoundItem as WorldItem;

                if (null != item)
                {
                    if (gridView.Columns[e.ColumnIndex].Name == columnDelete.Name)
                    {
                        if (gridView.Rows[e.RowIndex].Cells[e.ColumnIndex] is DataGridViewButtonCell)
                        {
                            try
                            {
                                this.tabControl.Enabled = false;

                                FileSystem.DeleteDirectory("." + Path.DirectorySeparatorChar + item.Name,
                                    UIOption.AllDialogs, RecycleOption.SendToRecycleBin, UICancelOption.DoNothing);

                                if (!File.Exists("." + Path.DirectorySeparatorChar + item.Name + Path.DirectorySeparatorChar + "session.lock")) //level.dat
                                {
                                    worldList.Remove(item);
                                    sortedWorlds.Remove(item.Name);


                                    SuspendBindingSourceEvents();

                                    worldList.ToList().ForEach(w => w.Selected = false);
                                    WorldItem.PrioritizeWorlds(worldList, registry);

                                    ResumeBindSourceEvents();

                                    WorldItem itm = comboBoxName.SelectedItem as WorldItem;
                                    if (null != itm && !itm.Selected)
                                    {
                                        itm.Selected = true;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                MessageBox.Show(this, "Nie można usunąć katalogu: " + item.Name + ". Sprawdź czy nie jest używany przez jakąś aplikację i ponów próbę.",
                                    "MineCraft Serwer",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            finally
                            {
                                this.tabControl.Enabled = true;
                            }
                        }
                    }
                }
            }
        }

        private void dataGridView_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            DataGridView gridView = sender as DataGridView;

            if (null != gridView && gridView.IsCurrentCellDirty)
            {
                DataGridViewCell cell = gridView.CurrentCell;

                if (null != cell && cell is DataGridViewCheckBoxCell)
                {
                    gridView.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
        }

        private void dataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView gridView = sender as DataGridView;

            if (null != gridView && 0 <= e.ColumnIndex && 0 <= e.RowIndex)
            {
                WorldItem item = gridView.Rows[e.RowIndex].DataBoundItem as WorldItem;

                if (null != item)
                {
                    if (gridView.Columns[e.ColumnIndex].Name == columnSelected.Name)
                    {
                        comboBoxName.SelectedItem = item;
                    }
                }
            }
        }

        private void dataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (0 <= e.ColumnIndex && 0 <= e.RowIndex)
            {
                WorldItem item = dataGridView.Rows[e.RowIndex].DataBoundItem as WorldItem;

                if (null != item)
                {
                    if (item.Selected)
                    {
                        dataGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = SystemColors.HotTrack;
                    }
                    else
                    {
                        dataGridView.Rows[e.RowIndex].DefaultCellStyle.BackColor = dataGridView.DefaultCellStyle.BackColor;
                    }
                }
            }
        }

        private void dataGridView_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView.ClearSelection();
        }

        private void dataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                row.HeaderCell.Value = Convert.ToString(row.Index + 1);
            }

            FavouritizeDeletButton();
        }

        private void toolStripMenuItemAddToFav_Click(object sender, EventArgs e)
        {
            Point location = dataGridView.PointToClient(contextMenuGrid.Bounds.Location);
            DataGridView.HitTestInfo hitTest = dataGridView.HitTest(location.X, location.Y);

            if (-1 < hitTest.RowIndex && hitTest.RowIndex < dataGridView.Rows.Count)
            {
                WorldItem item = dataGridView.Rows[hitTest.RowIndex].DataBoundItem as WorldItem;

                if (null != item && Directory.Exists(item.Name))
                {
                    if (toolStripMenuItemAddToFav.Tag.ToString() == ADD)
                    {
                        Stream str = null;
                        try
                        {
                            str = File.Create(item.Name + Path.DirectorySeparatorChar + "I.Like.It");
                            item.Favourite = true;
                        }
                        catch
                        {
                            //nothing
                        }
                        finally
                        {
                            if (null != str)
                                str.Close();
                        }
                    }
                    else if (File.Exists(item.Name + Path.DirectorySeparatorChar + "I.Like.It"))
                    {
                        try
                        {
                            File.Delete(item.Name + Path.DirectorySeparatorChar + "I.Like.It");
                            item.Favourite = false;
                        }
                        catch
                        {
                            //nothing
                        }
                    }
                }
            }

            FavouritizeDeletButton();
        }

        private void contextMenuGrid_Opening(object sender, CancelEventArgs e)
        {
            ContextMenuStrip contextMenu = sender as ContextMenuStrip;

            if (null != contextMenu)
            {                
                Point location = dataGridView.PointToClient(contextMenu.Bounds.Location);
                DataGridView.HitTestInfo hitTest = dataGridView.HitTest(location.X, location.Y);

                if (-1 < hitTest.RowIndex && hitTest.RowIndex < dataGridView.Rows.Count)
                {
                    WorldItem item = dataGridView.Rows[hitTest.RowIndex].DataBoundItem as WorldItem;

                    if (null != item)
                    {
                        if (item.Favourite)
                        {
                            toolStripMenuItemAddToFav.Text = "Usuń z ulubionych";
                            toolStripMenuItemAddToFav.Tag = DEL;
                            toolStripMenuItemAddToFav.Image = global::MinecraftMgr.Properties.Resources.minus_5_16;
                        }
                        else
                        {
                            toolStripMenuItemAddToFav.Text = "Dodaj do ulubionych";
                            toolStripMenuItemAddToFav.Tag = ADD;
                            toolStripMenuItemAddToFav.Image = global::MinecraftMgr.Properties.Resources.plus_5_16;
                        }
                    }
                }
            }
        }
        #endregion

        private void HideDeleteButton(bool hide)
        {
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
                WorldItem item = row.DataBoundItem as WorldItem;

                if (null != item)
                {
                    if (item.Selected && !item.Favourite)
                    {
                        if (hide)
                        {
                            row.Cells[columnDelete.Index] = new DataGridViewTextBoxCell();
                            row.Cells[columnDelete.Index].ReadOnly = true;
                        }
                        else
                        {
                            row.Cells[columnDelete.Index] = columnDelete.CellTemplate.Clone() as DataGridViewButtonCell;
                            row.Cells[columnDelete.Index].ReadOnly = false;
                        }
                    }
                }
            }
        }

        private void FavouritizeDeletButton()
        {
            foreach (DataGridViewRow row in dataGridView.Rows)
            {
               WorldItem item = row.DataBoundItem as WorldItem;

                if (null != item)
                {
                    if (item.Favourite)
                    {
                        row.Cells[columnDelete.Index] = new DataGridViewImageCell();
                        row.Cells[columnDelete.Index].Value = global::MinecraftMgr.Properties.Resources.favourite;
                        (row.Cells[columnDelete.Index] as DataGridViewImageCell).ImageLayout = DataGridViewImageCellLayout.Zoom;
                        row.Cells[columnDelete.Index].ToolTipText = "'" +item.Name + "' jest jednym z ulubionych, więc nie można go usunąć.";
                        row.Cells[columnDelete.Index].ReadOnly = true;
                    }
                    else if (IsMinecraftServerRunning && item.Selected)
                    {
                        row.Cells[columnDelete.Index] = new DataGridViewTextBoxCell();
                        row.Cells[columnDelete.Index].ReadOnly = true;
                    }
                    else 
                    {
                        row.Cells[columnDelete.Index] = columnDelete.CellTemplate.Clone() as DataGridViewButtonCell;
                        row.Cells[columnDelete.Index].ReadOnly = false;
                    }
                }
            }
        }
    }
}
