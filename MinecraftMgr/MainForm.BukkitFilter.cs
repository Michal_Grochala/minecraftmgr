﻿using MinecraftMgr.DiskOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinecraftMgr
{
    public partial class MainForm
    {
        private BukkitFilter bukkitFilterSettings = new BukkitFilter();

        #region Initialize/Finalize
        protected void ReadBukkitFilterSettings()
        {
            bukkitFilterSettings = registry.ReadBukkitFilterSettings();

            this.checkBoxActivateBukkit.Checked = bukkitFilterSettings.isActive;
            this.textBoxNetherSuffix.Text = bukkitFilterSettings.NetherSuffix;
            this.textBoxEndSuffix.Text = bukkitFilterSettings.EndSuffix;

            groupBoxSuffixes.Enabled = checkBoxActivateBukkit.Checked;
        }

        protected void SaveBukkitFilterSettings()
        {
            bukkitFilterSettings.isActive = this.checkBoxActivateBukkit.Checked;
            bukkitFilterSettings.EndSuffix = this.textBoxEndSuffix.Text.Trim();
            bukkitFilterSettings.NetherSuffix = this.textBoxNetherSuffix.Text.Trim();

            registry.WriteBukkintFilterSettings(bukkitFilterSettings);
        }
        #endregion

        #region Events
        private void buttonBukkitRefresh_Click(object sender, System.EventArgs e)
        {
            ReadBukkitFilterSettings();
        }

        private void buttonBukkitSave_Click(object sender, System.EventArgs e)
        {
            SaveBukkitFilterSettings();
        }

        private void checkBoxActivateBukkit_CheckedChanged(object sender, EventArgs e)
        {
            groupBoxSuffixes.Enabled = checkBoxActivateBukkit.Checked;
        }
        #endregion
    }
}
