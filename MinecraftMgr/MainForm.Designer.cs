﻿using System.Windows.Forms;
namespace MinecraftMgr
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.textBoxCommandLine = new System.Windows.Forms.TextBox();
            this.buttonStart = new System.Windows.Forms.Button();
            this.checkBoxOnlineMode = new System.Windows.Forms.CheckBox();
            this.checkBoxSpawnNpcs = new System.Windows.Forms.CheckBox();
            this.checkBoxSpawnMonsters = new System.Windows.Forms.CheckBox();
            this.checkBoxSpawnAnimals = new System.Windows.Forms.CheckBox();
            this.checkBoxAllowCommandBlock = new System.Windows.Forms.CheckBox();
            this.checkBoxGenerateStruct = new System.Windows.Forms.CheckBox();
            this.checkBoxAnnounceAchivements = new System.Windows.Forms.CheckBox();
            this.checkBoxPvp = new System.Windows.Forms.CheckBox();
            this.comboBoxLevelType = new System.Windows.Forms.ComboBox();
            this.comboBoxDifficulty = new System.Windows.Forms.ComboBox();
            this.checkBoxAllowNether = new System.Windows.Forms.CheckBox();
            this.checkBoxAllowFly = new System.Windows.Forms.CheckBox();
            this.checkBoxForceGamemode = new System.Windows.Forms.CheckBox();
            this.comboBoxGamemode = new System.Windows.Forms.ComboBox();
            this.comboBoxName = new System.Windows.Forms.ComboBox();
            this.bindingSourceCombo = new System.Windows.Forms.BindingSource(this.components);
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonServerCommand = new System.Windows.Forms.Button();
            this.buttonServerRefresh = new System.Windows.Forms.Button();
            this.comboBoxServerName = new System.Windows.Forms.ComboBox();
            this.buttonJavaSave = new System.Windows.Forms.Button();
            this.buttonJavaRefresh = new System.Windows.Forms.Button();
            this.buttonBukkitRefresh = new System.Windows.Forms.Button();
            this.buttonBukkitSave = new System.Windows.Forms.Button();
            this.textBoxUserNumber = new System.Windows.Forms.TextBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelPlayerNum = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelLevelType = new System.Windows.Forms.Label();
            this.labelDifficulty = new System.Windows.Forms.Label();
            this.labelGamemode = new System.Windows.Forms.Label();
            this.tabPageRemoveWorld = new System.Windows.Forms.TabPage();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.columnPriorityIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.columnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemAddToFav = new System.Windows.Forms.ToolStripMenuItem();
            this.columnSelected = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.columnDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.bindingSourceGrid = new System.Windows.Forms.BindingSource(this.components);
            this.tabPageLogs = new System.Windows.Forms.TabPage();
            this.richTextBoxLogs = new System.Windows.Forms.RichTextBox();
            this.contextMenuLog = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemClear = new System.Windows.Forms.ToolStripMenuItem();
            this.panelDontDisplayCmndBlock = new System.Windows.Forms.Panel();
            this.checkBoxSkipCommandBlocks = new System.Windows.Forms.CheckBox();
            this.tabPageJava = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxOtherParms = new System.Windows.Forms.TextBox();
            this.textBoxXmx = new System.Windows.Forms.TextBox();
            this.labelXmx = new System.Windows.Forms.Label();
            this.textBoxXms = new System.Windows.Forms.TextBox();
            this.labelXms = new System.Windows.Forms.Label();
            this.comboBoxJavaExe = new System.Windows.Forms.ComboBox();
            this.tabPageBukkit = new System.Windows.Forms.TabPage();
            this.panelBukkitFilter = new System.Windows.Forms.Panel();
            this.groupBoxSuffixes = new System.Windows.Forms.GroupBox();
            this.textBoxNetherSuffix = new System.Windows.Forms.TextBox();
            this.textBoxEndSuffix = new System.Windows.Forms.TextBox();
            this.labelNetherSuffix = new System.Windows.Forms.Label();
            this.labelEndSuffix = new System.Windows.Forms.Label();
            this.checkBoxActivateBukkit = new System.Windows.Forms.CheckBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelCommandLine = new System.Windows.Forms.Label();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.minecraftProcess = new System.Diagnostics.Process();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.panelTop = new System.Windows.Forms.Panel();
            this.panelCenter = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCombo)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPageRemoveWorld.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.contextMenuGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceGrid)).BeginInit();
            this.tabPageLogs.SuspendLayout();
            this.contextMenuLog.SuspendLayout();
            this.panelDontDisplayCmndBlock.SuspendLayout();
            this.tabPageJava.SuspendLayout();
            this.tabPageBukkit.SuspendLayout();
            this.panelBukkitFilter.SuspendLayout();
            this.groupBoxSuffixes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.panelTop.SuspendLayout();
            this.panelCenter.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxCommandLine
            // 
            resources.ApplyResources(this.textBoxCommandLine, "textBoxCommandLine");
            this.textBoxCommandLine.Name = "textBoxCommandLine";
            this.toolTip.SetToolTip(this.textBoxCommandLine, resources.GetString("textBoxCommandLine.ToolTip"));
            this.textBoxCommandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxCommandLine_KeyDown);
            // 
            // buttonStart
            // 
            resources.ApplyResources(this.buttonStart, "buttonStart");
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Tag = "START";
            this.toolTip.SetToolTip(this.buttonStart, resources.GetString("buttonStart.ToolTip"));
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // checkBoxOnlineMode
            // 
            resources.ApplyResources(this.checkBoxOnlineMode, "checkBoxOnlineMode");
            this.checkBoxOnlineMode.Name = "checkBoxOnlineMode";
            this.checkBoxOnlineMode.Tag = "online-mode";
            this.toolTip.SetToolTip(this.checkBoxOnlineMode, resources.GetString("checkBoxOnlineMode.ToolTip"));
            this.checkBoxOnlineMode.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpawnNpcs
            // 
            resources.ApplyResources(this.checkBoxSpawnNpcs, "checkBoxSpawnNpcs");
            this.checkBoxSpawnNpcs.Checked = true;
            this.checkBoxSpawnNpcs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSpawnNpcs.Name = "checkBoxSpawnNpcs";
            this.checkBoxSpawnNpcs.Tag = "spawn-npcs";
            this.toolTip.SetToolTip(this.checkBoxSpawnNpcs, resources.GetString("checkBoxSpawnNpcs.ToolTip"));
            this.checkBoxSpawnNpcs.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpawnMonsters
            // 
            resources.ApplyResources(this.checkBoxSpawnMonsters, "checkBoxSpawnMonsters");
            this.checkBoxSpawnMonsters.Checked = true;
            this.checkBoxSpawnMonsters.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSpawnMonsters.Name = "checkBoxSpawnMonsters";
            this.checkBoxSpawnMonsters.Tag = "spawn-monsters";
            this.toolTip.SetToolTip(this.checkBoxSpawnMonsters, resources.GetString("checkBoxSpawnMonsters.ToolTip"));
            this.checkBoxSpawnMonsters.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpawnAnimals
            // 
            resources.ApplyResources(this.checkBoxSpawnAnimals, "checkBoxSpawnAnimals");
            this.checkBoxSpawnAnimals.Checked = true;
            this.checkBoxSpawnAnimals.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSpawnAnimals.Name = "checkBoxSpawnAnimals";
            this.checkBoxSpawnAnimals.Tag = "spawn-animals";
            this.toolTip.SetToolTip(this.checkBoxSpawnAnimals, resources.GetString("checkBoxSpawnAnimals.ToolTip"));
            this.checkBoxSpawnAnimals.UseVisualStyleBackColor = true;
            // 
            // checkBoxAllowCommandBlock
            // 
            resources.ApplyResources(this.checkBoxAllowCommandBlock, "checkBoxAllowCommandBlock");
            this.checkBoxAllowCommandBlock.Checked = true;
            this.checkBoxAllowCommandBlock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAllowCommandBlock.Name = "checkBoxAllowCommandBlock";
            this.checkBoxAllowCommandBlock.Tag = "enable-command-block";
            this.toolTip.SetToolTip(this.checkBoxAllowCommandBlock, resources.GetString("checkBoxAllowCommandBlock.ToolTip"));
            this.checkBoxAllowCommandBlock.UseVisualStyleBackColor = true;
            // 
            // checkBoxGenerateStruct
            // 
            resources.ApplyResources(this.checkBoxGenerateStruct, "checkBoxGenerateStruct");
            this.checkBoxGenerateStruct.Checked = true;
            this.checkBoxGenerateStruct.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGenerateStruct.Name = "checkBoxGenerateStruct";
            this.checkBoxGenerateStruct.Tag = "generate-structures";
            this.toolTip.SetToolTip(this.checkBoxGenerateStruct, resources.GetString("checkBoxGenerateStruct.ToolTip"));
            this.checkBoxGenerateStruct.UseVisualStyleBackColor = true;
            // 
            // checkBoxAnnounceAchivements
            // 
            resources.ApplyResources(this.checkBoxAnnounceAchivements, "checkBoxAnnounceAchivements");
            this.checkBoxAnnounceAchivements.Checked = true;
            this.checkBoxAnnounceAchivements.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAnnounceAchivements.Name = "checkBoxAnnounceAchivements";
            this.checkBoxAnnounceAchivements.Tag = "announce-player-achievements";
            this.toolTip.SetToolTip(this.checkBoxAnnounceAchivements, resources.GetString("checkBoxAnnounceAchivements.ToolTip"));
            this.checkBoxAnnounceAchivements.UseVisualStyleBackColor = true;
            // 
            // checkBoxPvp
            // 
            resources.ApplyResources(this.checkBoxPvp, "checkBoxPvp");
            this.checkBoxPvp.Checked = true;
            this.checkBoxPvp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPvp.Name = "checkBoxPvp";
            this.checkBoxPvp.Tag = "pvp";
            this.toolTip.SetToolTip(this.checkBoxPvp, resources.GetString("checkBoxPvp.ToolTip"));
            this.checkBoxPvp.UseVisualStyleBackColor = true;
            // 
            // comboBoxLevelType
            // 
            this.comboBoxLevelType.DisplayMember = "0 - Przetrwanie";
            this.comboBoxLevelType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLevelType.Items.AddRange(new object[] {
            resources.GetString("comboBoxLevelType.Items"),
            resources.GetString("comboBoxLevelType.Items1"),
            resources.GetString("comboBoxLevelType.Items2"),
            resources.GetString("comboBoxLevelType.Items3")});
            resources.ApplyResources(this.comboBoxLevelType, "comboBoxLevelType");
            this.comboBoxLevelType.Name = "comboBoxLevelType";
            this.comboBoxLevelType.Tag = "level-type";
            this.toolTip.SetToolTip(this.comboBoxLevelType, resources.GetString("comboBoxLevelType.ToolTip"));
            // 
            // comboBoxDifficulty
            // 
            this.comboBoxDifficulty.DisplayMember = "0 - Przetrwanie";
            this.comboBoxDifficulty.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDifficulty.Items.AddRange(new object[] {
            resources.GetString("comboBoxDifficulty.Items"),
            resources.GetString("comboBoxDifficulty.Items1"),
            resources.GetString("comboBoxDifficulty.Items2"),
            resources.GetString("comboBoxDifficulty.Items3")});
            resources.ApplyResources(this.comboBoxDifficulty, "comboBoxDifficulty");
            this.comboBoxDifficulty.Name = "comboBoxDifficulty";
            this.comboBoxDifficulty.Tag = "difficulty";
            this.toolTip.SetToolTip(this.comboBoxDifficulty, resources.GetString("comboBoxDifficulty.ToolTip"));
            // 
            // checkBoxAllowNether
            // 
            resources.ApplyResources(this.checkBoxAllowNether, "checkBoxAllowNether");
            this.checkBoxAllowNether.Checked = true;
            this.checkBoxAllowNether.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAllowNether.Name = "checkBoxAllowNether";
            this.checkBoxAllowNether.Tag = "allow-nether";
            this.toolTip.SetToolTip(this.checkBoxAllowNether, resources.GetString("checkBoxAllowNether.ToolTip"));
            this.checkBoxAllowNether.UseVisualStyleBackColor = true;
            // 
            // checkBoxAllowFly
            // 
            resources.ApplyResources(this.checkBoxAllowFly, "checkBoxAllowFly");
            this.checkBoxAllowFly.Name = "checkBoxAllowFly";
            this.checkBoxAllowFly.Tag = "allow-flight";
            this.toolTip.SetToolTip(this.checkBoxAllowFly, resources.GetString("checkBoxAllowFly.ToolTip"));
            this.checkBoxAllowFly.UseVisualStyleBackColor = true;
            // 
            // checkBoxForceGamemode
            // 
            resources.ApplyResources(this.checkBoxForceGamemode, "checkBoxForceGamemode");
            this.checkBoxForceGamemode.Checked = true;
            this.checkBoxForceGamemode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxForceGamemode.Name = "checkBoxForceGamemode";
            this.checkBoxForceGamemode.Tag = "force-gamemode";
            this.toolTip.SetToolTip(this.checkBoxForceGamemode, resources.GetString("checkBoxForceGamemode.ToolTip"));
            this.checkBoxForceGamemode.UseVisualStyleBackColor = true;
            // 
            // comboBoxGamemode
            // 
            this.comboBoxGamemode.DisplayMember = "0 - Przetrwanie";
            this.comboBoxGamemode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxGamemode.Items.AddRange(new object[] {
            resources.GetString("comboBoxGamemode.Items"),
            resources.GetString("comboBoxGamemode.Items1"),
            resources.GetString("comboBoxGamemode.Items2"),
            resources.GetString("comboBoxGamemode.Items3")});
            resources.ApplyResources(this.comboBoxGamemode, "comboBoxGamemode");
            this.comboBoxGamemode.Name = "comboBoxGamemode";
            this.comboBoxGamemode.Tag = "gamemode";
            this.toolTip.SetToolTip(this.comboBoxGamemode, resources.GetString("comboBoxGamemode.ToolTip"));
            // 
            // comboBoxName
            // 
            this.comboBoxName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxName.DataSource = this.bindingSourceCombo;
            this.comboBoxName.DisplayMember = "Name";
            this.comboBoxName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            resources.ApplyResources(this.comboBoxName, "comboBoxName");
            this.comboBoxName.Name = "comboBoxName";
            this.comboBoxName.Tag = "level-name";
            this.toolTip.SetToolTip(this.comboBoxName, resources.GetString("comboBoxName.ToolTip"));
            this.comboBoxName.SelectedIndexChanged += new System.EventHandler(this.comboBoxName_SelectedIndexChanged);
            this.comboBoxName.Leave += new System.EventHandler(this.comboBoxName_Leave);
            // 
            // bindingSourceCombo
            // 
            this.bindingSourceCombo.DataSource = typeof(MinecraftMgr.Data.WorldItem);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Image = global::MinecraftMgr.Properties.Resources.refresh_16;
            resources.ApplyResources(this.buttonRefresh, "buttonRefresh");
            this.buttonRefresh.Name = "buttonRefresh";
            this.toolTip.SetToolTip(this.buttonRefresh, resources.GetString("buttonRefresh.ToolTip"));
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonServerCommand
            // 
            resources.ApplyResources(this.buttonServerCommand, "buttonServerCommand");
            this.buttonServerCommand.Image = global::MinecraftMgr.Properties.Resources.arrow_192_16;
            this.buttonServerCommand.Name = "buttonServerCommand";
            this.toolTip.SetToolTip(this.buttonServerCommand, resources.GetString("buttonServerCommand.ToolTip"));
            this.buttonServerCommand.UseVisualStyleBackColor = true;
            this.buttonServerCommand.Click += new System.EventHandler(this.buttonServerCommand_Click);
            // 
            // buttonServerRefresh
            // 
            this.buttonServerRefresh.Image = global::MinecraftMgr.Properties.Resources.sinchronize_16;
            resources.ApplyResources(this.buttonServerRefresh, "buttonServerRefresh");
            this.buttonServerRefresh.Name = "buttonServerRefresh";
            this.toolTip.SetToolTip(this.buttonServerRefresh, resources.GetString("buttonServerRefresh.ToolTip"));
            this.buttonServerRefresh.UseVisualStyleBackColor = true;
            this.buttonServerRefresh.Click += new System.EventHandler(this.buttonServerRefresh_Click);
            // 
            // comboBoxServerName
            // 
            this.comboBoxServerName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxServerName.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxServerName, "comboBoxServerName");
            this.comboBoxServerName.Name = "comboBoxServerName";
            this.toolTip.SetToolTip(this.comboBoxServerName, resources.GetString("comboBoxServerName.ToolTip"));
            // 
            // buttonJavaSave
            // 
            this.buttonJavaSave.Image = global::MinecraftMgr.Properties.Resources.save_16;
            resources.ApplyResources(this.buttonJavaSave, "buttonJavaSave");
            this.buttonJavaSave.Name = "buttonJavaSave";
            this.toolTip.SetToolTip(this.buttonJavaSave, resources.GetString("buttonJavaSave.ToolTip"));
            this.buttonJavaSave.UseVisualStyleBackColor = true;
            this.buttonJavaSave.Click += new System.EventHandler(this.buttonJavaSave_Click);
            // 
            // buttonJavaRefresh
            // 
            this.buttonJavaRefresh.Image = global::MinecraftMgr.Properties.Resources.refresh_16;
            resources.ApplyResources(this.buttonJavaRefresh, "buttonJavaRefresh");
            this.buttonJavaRefresh.Name = "buttonJavaRefresh";
            this.toolTip.SetToolTip(this.buttonJavaRefresh, resources.GetString("buttonJavaRefresh.ToolTip"));
            this.buttonJavaRefresh.UseVisualStyleBackColor = true;
            this.buttonJavaRefresh.Click += new System.EventHandler(this.buttonJavaRefresh_Click);
            // 
            // buttonBukkitRefresh
            // 
            this.buttonBukkitRefresh.Image = global::MinecraftMgr.Properties.Resources.refresh_16;
            resources.ApplyResources(this.buttonBukkitRefresh, "buttonBukkitRefresh");
            this.buttonBukkitRefresh.Name = "buttonBukkitRefresh";
            this.toolTip.SetToolTip(this.buttonBukkitRefresh, resources.GetString("buttonBukkitRefresh.ToolTip"));
            this.buttonBukkitRefresh.UseVisualStyleBackColor = true;
            this.buttonBukkitRefresh.Click += new System.EventHandler(this.buttonBukkitRefresh_Click);
            // 
            // buttonBukkitSave
            // 
            this.buttonBukkitSave.Image = global::MinecraftMgr.Properties.Resources.save_16;
            resources.ApplyResources(this.buttonBukkitSave, "buttonBukkitSave");
            this.buttonBukkitSave.Name = "buttonBukkitSave";
            this.toolTip.SetToolTip(this.buttonBukkitSave, resources.GetString("buttonBukkitSave.ToolTip"));
            this.buttonBukkitSave.UseVisualStyleBackColor = true;
            this.buttonBukkitSave.Click += new System.EventHandler(this.buttonBukkitSave_Click);
            // 
            // textBoxUserNumber
            // 
            resources.ApplyResources(this.textBoxUserNumber, "textBoxUserNumber");
            this.textBoxUserNumber.Name = "textBoxUserNumber";
            this.textBoxUserNumber.Tag = "max-players";
            this.toolTip.SetToolTip(this.textBoxUserNumber, resources.GetString("textBoxUserNumber.ToolTip"));
            this.textBoxUserNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxUserNumber_KeyUp);
            this.textBoxUserNumber.Validated += new System.EventHandler(this.textBoxUserNumber_Validated);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageSettings);
            this.tabControl.Controls.Add(this.tabPageRemoveWorld);
            this.tabControl.Controls.Add(this.tabPageLogs);
            this.tabControl.Controls.Add(this.tabPageJava);
            this.tabControl.Controls.Add(this.tabPageBukkit);
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Controls.Add(this.groupBox2);
            this.tabPageSettings.Controls.Add(this.groupBox1);
            resources.ApplyResources(this.tabPageSettings, "tabPageSettings");
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelPlayerNum);
            this.groupBox2.Controls.Add(this.textBoxUserNumber);
            this.groupBox2.Controls.Add(this.checkBoxOnlineMode);
            this.groupBox2.Controls.Add(this.checkBoxSpawnNpcs);
            this.groupBox2.Controls.Add(this.checkBoxSpawnMonsters);
            this.groupBox2.Controls.Add(this.checkBoxSpawnAnimals);
            this.groupBox2.Controls.Add(this.checkBoxAllowCommandBlock);
            this.groupBox2.Controls.Add(this.checkBoxGenerateStruct);
            this.groupBox2.Controls.Add(this.checkBoxAnnounceAchivements);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // labelPlayerNum
            // 
            resources.ApplyResources(this.labelPlayerNum, "labelPlayerNum");
            this.labelPlayerNum.Name = "labelPlayerNum";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxPvp);
            this.groupBox1.Controls.Add(this.comboBoxLevelType);
            this.groupBox1.Controls.Add(this.labelLevelType);
            this.groupBox1.Controls.Add(this.comboBoxDifficulty);
            this.groupBox1.Controls.Add(this.labelDifficulty);
            this.groupBox1.Controls.Add(this.checkBoxAllowNether);
            this.groupBox1.Controls.Add(this.checkBoxAllowFly);
            this.groupBox1.Controls.Add(this.checkBoxForceGamemode);
            this.groupBox1.Controls.Add(this.comboBoxGamemode);
            this.groupBox1.Controls.Add(this.labelGamemode);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // labelLevelType
            // 
            resources.ApplyResources(this.labelLevelType, "labelLevelType");
            this.labelLevelType.Name = "labelLevelType";
            // 
            // labelDifficulty
            // 
            resources.ApplyResources(this.labelDifficulty, "labelDifficulty");
            this.labelDifficulty.Name = "labelDifficulty";
            // 
            // labelGamemode
            // 
            resources.ApplyResources(this.labelGamemode, "labelGamemode");
            this.labelGamemode.Name = "labelGamemode";
            // 
            // tabPageRemoveWorld
            // 
            this.tabPageRemoveWorld.Controls.Add(this.dataGridView);
            resources.ApplyResources(this.tabPageRemoveWorld, "tabPageRemoveWorld");
            this.tabPageRemoveWorld.Name = "tabPageRemoveWorld";
            this.tabPageRemoveWorld.UseVisualStyleBackColor = true;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.AutoGenerateColumns = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnPriorityIcon,
            this.columnName,
            this.columnSelected,
            this.columnDelete});
            this.dataGridView.DataSource = this.bindingSourceGrid;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Transparent;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.dataGridView, "dataGridView");
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
            this.dataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridView_CellFormatting);
            this.dataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellValueChanged);
            this.dataGridView.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView_CurrentCellDirtyStateChanged);
            this.dataGridView.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView_DataBindingComplete);
            this.dataGridView.SelectionChanged += new System.EventHandler(this.dataGridView_SelectionChanged);
            // 
            // columnPriorityIcon
            // 
            this.columnPriorityIcon.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.columnPriorityIcon.DataPropertyName = "Image";
            resources.ApplyResources(this.columnPriorityIcon, "columnPriorityIcon");
            this.columnPriorityIcon.Image = global::MinecraftMgr.Properties.Resources.Empty;
            this.columnPriorityIcon.Name = "columnPriorityIcon";
            this.columnPriorityIcon.ReadOnly = true;
            this.columnPriorityIcon.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // columnName
            // 
            this.columnName.ContextMenuStrip = this.contextMenuGrid;
            this.columnName.DataPropertyName = "Name";
            resources.ApplyResources(this.columnName, "columnName");
            this.columnName.Name = "columnName";
            this.columnName.ReadOnly = true;
            // 
            // contextMenuGrid
            // 
            this.contextMenuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemAddToFav});
            this.contextMenuGrid.Name = "contextMenuGrid";
            resources.ApplyResources(this.contextMenuGrid, "contextMenuGrid");
            this.contextMenuGrid.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuGrid_Opening);
            // 
            // toolStripMenuItemAddToFav
            // 
            this.toolStripMenuItemAddToFav.Name = "toolStripMenuItemAddToFav";
            resources.ApplyResources(this.toolStripMenuItemAddToFav, "toolStripMenuItemAddToFav");
            this.toolStripMenuItemAddToFav.Click += new System.EventHandler(this.toolStripMenuItemAddToFav_Click);
            // 
            // columnSelected
            // 
            this.columnSelected.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.columnSelected.DataPropertyName = "Selected";
            resources.ApplyResources(this.columnSelected, "columnSelected");
            this.columnSelected.Name = "columnSelected";
            // 
            // columnDelete
            // 
            resources.ApplyResources(this.columnDelete, "columnDelete");
            this.columnDelete.Name = "columnDelete";
            this.columnDelete.Text = "Usuń";
            this.columnDelete.UseColumnTextForButtonValue = true;
            // 
            // bindingSourceGrid
            // 
            this.bindingSourceGrid.DataSource = typeof(MinecraftMgr.Data.WorldItem);
            // 
            // tabPageLogs
            // 
            this.tabPageLogs.Controls.Add(this.richTextBoxLogs);
            this.tabPageLogs.Controls.Add(this.panelDontDisplayCmndBlock);
            resources.ApplyResources(this.tabPageLogs, "tabPageLogs");
            this.tabPageLogs.Name = "tabPageLogs";
            this.tabPageLogs.UseVisualStyleBackColor = true;
            // 
            // richTextBoxLogs
            // 
            this.richTextBoxLogs.ContextMenuStrip = this.contextMenuLog;
            resources.ApplyResources(this.richTextBoxLogs, "richTextBoxLogs");
            this.richTextBoxLogs.HideSelection = false;
            this.richTextBoxLogs.Name = "richTextBoxLogs";
            this.richTextBoxLogs.ReadOnly = true;
            // 
            // contextMenuLog
            // 
            this.contextMenuLog.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemClear});
            this.contextMenuLog.Name = "contextMenuStrip";
            resources.ApplyResources(this.contextMenuLog, "contextMenuLog");
            // 
            // toolStripMenuItemClear
            // 
            this.toolStripMenuItemClear.Name = "toolStripMenuItemClear";
            resources.ApplyResources(this.toolStripMenuItemClear, "toolStripMenuItemClear");
            this.toolStripMenuItemClear.Click += new System.EventHandler(this.toolStripMenuItemClear_Click);
            // 
            // panelDontDisplayCmndBlock
            // 
            this.panelDontDisplayCmndBlock.Controls.Add(this.checkBoxSkipCommandBlocks);
            resources.ApplyResources(this.panelDontDisplayCmndBlock, "panelDontDisplayCmndBlock");
            this.panelDontDisplayCmndBlock.Name = "panelDontDisplayCmndBlock";
            // 
            // checkBoxSkipCommandBlocks
            // 
            resources.ApplyResources(this.checkBoxSkipCommandBlocks, "checkBoxSkipCommandBlocks");
            this.checkBoxSkipCommandBlocks.Checked = true;
            this.checkBoxSkipCommandBlocks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSkipCommandBlocks.Name = "checkBoxSkipCommandBlocks";
            this.checkBoxSkipCommandBlocks.UseVisualStyleBackColor = true;
            // 
            // tabPageJava
            // 
            this.tabPageJava.Controls.Add(this.buttonJavaRefresh);
            this.tabPageJava.Controls.Add(this.buttonJavaSave);
            this.tabPageJava.Controls.Add(this.buttonServerRefresh);
            this.tabPageJava.Controls.Add(this.comboBoxServerName);
            this.tabPageJava.Controls.Add(this.label1);
            this.tabPageJava.Controls.Add(this.textBoxOtherParms);
            this.tabPageJava.Controls.Add(this.textBoxXmx);
            this.tabPageJava.Controls.Add(this.labelXmx);
            this.tabPageJava.Controls.Add(this.textBoxXms);
            this.tabPageJava.Controls.Add(this.labelXms);
            this.tabPageJava.Controls.Add(this.comboBoxJavaExe);
            resources.ApplyResources(this.tabPageJava, "tabPageJava");
            this.tabPageJava.Name = "tabPageJava";
            this.tabPageJava.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // textBoxOtherParms
            // 
            resources.ApplyResources(this.textBoxOtherParms, "textBoxOtherParms");
            this.textBoxOtherParms.Name = "textBoxOtherParms";
            // 
            // textBoxXmx
            // 
            resources.ApplyResources(this.textBoxXmx, "textBoxXmx");
            this.textBoxXmx.Name = "textBoxXmx";
            // 
            // labelXmx
            // 
            resources.ApplyResources(this.labelXmx, "labelXmx");
            this.labelXmx.Name = "labelXmx";
            // 
            // textBoxXms
            // 
            resources.ApplyResources(this.textBoxXms, "textBoxXms");
            this.textBoxXms.Name = "textBoxXms";
            // 
            // labelXms
            // 
            resources.ApplyResources(this.labelXms, "labelXms");
            this.labelXms.Name = "labelXms";
            // 
            // comboBoxJavaExe
            // 
            this.comboBoxJavaExe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxJavaExe.Items.AddRange(new object[] {
            resources.GetString("comboBoxJavaExe.Items"),
            resources.GetString("comboBoxJavaExe.Items1")});
            resources.ApplyResources(this.comboBoxJavaExe, "comboBoxJavaExe");
            this.comboBoxJavaExe.Name = "comboBoxJavaExe";
            // 
            // tabPageBukkit
            // 
            this.tabPageBukkit.Controls.Add(this.panelBukkitFilter);
            resources.ApplyResources(this.tabPageBukkit, "tabPageBukkit");
            this.tabPageBukkit.Name = "tabPageBukkit";
            this.tabPageBukkit.UseVisualStyleBackColor = true;
            // 
            // panelBukkitFilter
            // 
            this.panelBukkitFilter.Controls.Add(this.groupBoxSuffixes);
            this.panelBukkitFilter.Controls.Add(this.checkBoxActivateBukkit);
            this.panelBukkitFilter.Controls.Add(this.buttonBukkitRefresh);
            this.panelBukkitFilter.Controls.Add(this.buttonBukkitSave);
            resources.ApplyResources(this.panelBukkitFilter, "panelBukkitFilter");
            this.panelBukkitFilter.Name = "panelBukkitFilter";
            // 
            // groupBoxSuffixes
            // 
            this.groupBoxSuffixes.Controls.Add(this.textBoxNetherSuffix);
            this.groupBoxSuffixes.Controls.Add(this.textBoxEndSuffix);
            this.groupBoxSuffixes.Controls.Add(this.labelNetherSuffix);
            this.groupBoxSuffixes.Controls.Add(this.labelEndSuffix);
            resources.ApplyResources(this.groupBoxSuffixes, "groupBoxSuffixes");
            this.groupBoxSuffixes.Name = "groupBoxSuffixes";
            this.groupBoxSuffixes.TabStop = false;
            // 
            // textBoxNetherSuffix
            // 
            resources.ApplyResources(this.textBoxNetherSuffix, "textBoxNetherSuffix");
            this.textBoxNetherSuffix.Name = "textBoxNetherSuffix";
            // 
            // textBoxEndSuffix
            // 
            resources.ApplyResources(this.textBoxEndSuffix, "textBoxEndSuffix");
            this.textBoxEndSuffix.Name = "textBoxEndSuffix";
            // 
            // labelNetherSuffix
            // 
            resources.ApplyResources(this.labelNetherSuffix, "labelNetherSuffix");
            this.labelNetherSuffix.Name = "labelNetherSuffix";
            // 
            // labelEndSuffix
            // 
            resources.ApplyResources(this.labelEndSuffix, "labelEndSuffix");
            this.labelEndSuffix.Name = "labelEndSuffix";
            // 
            // checkBoxActivateBukkit
            // 
            resources.ApplyResources(this.checkBoxActivateBukkit, "checkBoxActivateBukkit");
            this.checkBoxActivateBukkit.Name = "checkBoxActivateBukkit";
            this.checkBoxActivateBukkit.UseVisualStyleBackColor = true;
            this.checkBoxActivateBukkit.CheckedChanged += new System.EventHandler(this.checkBoxActivateBukkit_CheckedChanged);
            // 
            // labelName
            // 
            resources.ApplyResources(this.labelName, "labelName");
            this.labelName.Name = "labelName";
            // 
            // labelCommandLine
            // 
            resources.ApplyResources(this.labelCommandLine, "labelCommandLine");
            this.labelCommandLine.Name = "labelCommandLine";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.minecraftProcessStart);
            // 
            // minecraftProcess
            // 
            this.minecraftProcess.EnableRaisingEvents = true;
            this.minecraftProcess.StartInfo.CreateNoWindow = true;
            this.minecraftProcess.StartInfo.Domain = "";
            this.minecraftProcess.StartInfo.FileName = "javaw";
            this.minecraftProcess.StartInfo.LoadUserProfile = false;
            this.minecraftProcess.StartInfo.Password = null;
            this.minecraftProcess.StartInfo.RedirectStandardError = true;
            this.minecraftProcess.StartInfo.RedirectStandardInput = true;
            this.minecraftProcess.StartInfo.RedirectStandardOutput = true;
            this.minecraftProcess.StartInfo.StandardErrorEncoding = null;
            this.minecraftProcess.StartInfo.StandardOutputEncoding = null;
            this.minecraftProcess.StartInfo.UserName = "";
            this.minecraftProcess.StartInfo.UseShellExecute = false;
            this.minecraftProcess.SynchronizingObject = this;
            this.minecraftProcess.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(this.minecraftProcess_OutputDataReceived);
            this.minecraftProcess.ErrorDataReceived += new System.Diagnostics.DataReceivedEventHandler(this.minecraftProcess_ErrorDataReceived);
            this.minecraftProcess.Exited += new System.EventHandler(this.minecraftProcess_Exited);
            // 
            // progressBar
            // 
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.Name = "progressBar";
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.buttonRefresh);
            this.panelTop.Controls.Add(this.comboBoxName);
            this.panelTop.Controls.Add(this.labelName);
            resources.ApplyResources(this.panelTop, "panelTop");
            this.panelTop.Name = "panelTop";
            // 
            // panelCenter
            // 
            this.panelCenter.Controls.Add(this.buttonServerCommand);
            this.panelCenter.Controls.Add(this.buttonStart);
            this.panelCenter.Controls.Add(this.textBoxCommandLine);
            this.panelCenter.Controls.Add(this.labelCommandLine);
            this.panelCenter.Controls.Add(this.tabControl);
            resources.ApplyResources(this.panelCenter, "panelCenter");
            this.panelCenter.Name = "panelCenter";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelCenter);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.progressBar);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceCombo)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tabPageSettings.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPageRemoveWorld.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.contextMenuGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceGrid)).EndInit();
            this.tabPageLogs.ResumeLayout(false);
            this.contextMenuLog.ResumeLayout(false);
            this.panelDontDisplayCmndBlock.ResumeLayout(false);
            this.panelDontDisplayCmndBlock.PerformLayout();
            this.tabPageJava.ResumeLayout(false);
            this.tabPageJava.PerformLayout();
            this.tabPageBukkit.ResumeLayout(false);
            this.panelBukkitFilter.ResumeLayout(false);
            this.panelBukkitFilter.PerformLayout();
            this.groupBoxSuffixes.ResumeLayout(false);
            this.groupBoxSuffixes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panelCenter.ResumeLayout(false);
            this.panelCenter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.TabPage tabPageLogs;
        private System.Windows.Forms.Label labelCommandLine;
        private System.Windows.Forms.TextBox textBoxCommandLine;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxOnlineMode;
        private System.Windows.Forms.CheckBox checkBoxSpawnNpcs;
        private System.Windows.Forms.CheckBox checkBoxSpawnMonsters;
        private System.Windows.Forms.CheckBox checkBoxSpawnAnimals;
        private System.Windows.Forms.CheckBox checkBoxAllowCommandBlock;
        private System.Windows.Forms.CheckBox checkBoxGenerateStruct;
        private System.Windows.Forms.CheckBox checkBoxAnnounceAchivements;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxPvp;
        private System.Windows.Forms.ComboBox comboBoxLevelType;
        private System.Windows.Forms.Label labelLevelType;
        private System.Windows.Forms.ComboBox comboBoxDifficulty;
        private System.Windows.Forms.Label labelDifficulty;
        private System.Windows.Forms.CheckBox checkBoxAllowNether;
        private System.Windows.Forms.CheckBox checkBoxAllowFly;
        private System.Windows.Forms.CheckBox checkBoxForceGamemode;
        private System.Windows.Forms.ComboBox comboBoxGamemode;
        private System.Windows.Forms.Label labelGamemode;
        private System.Windows.Forms.ComboBox comboBoxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.TabPage tabPageRemoveWorld;
        private System.Windows.Forms.RichTextBox richTextBoxLogs;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.BindingSource bindingSourceGrid;
        private System.Windows.Forms.BindingSource bindingSourceCombo;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonServerCommand;
        private System.Windows.Forms.ContextMenuStrip contextMenuLog;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemClear;
        private System.Diagnostics.Process minecraftProcess;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel panelCenter;
        private System.Windows.Forms.TabPage tabPageJava;
        private System.Windows.Forms.ComboBox comboBoxJavaExe;
        private System.Windows.Forms.Label labelXms;
        private System.Windows.Forms.TextBox textBoxXms;
        private System.Windows.Forms.TextBox textBoxXmx;
        private System.Windows.Forms.Label labelXmx;
        private System.Windows.Forms.TextBox textBoxOtherParms;
        private System.Windows.Forms.ComboBox comboBoxServerName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonServerRefresh;
        private System.Windows.Forms.Button buttonJavaSave;
        private System.Windows.Forms.Button buttonJavaRefresh;
        private System.Windows.Forms.ContextMenuStrip contextMenuGrid;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAddToFav;
        private System.Windows.Forms.DataGridViewImageColumn columnPriorityIcon;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn columnSelected;
        private System.Windows.Forms.DataGridViewButtonColumn columnDelete;
        private System.Windows.Forms.TabPage tabPageBukkit;
        private System.Windows.Forms.Panel panelBukkitFilter;
        private System.Windows.Forms.Button buttonBukkitRefresh;
        private System.Windows.Forms.Button buttonBukkitSave;
        private System.Windows.Forms.CheckBox checkBoxActivateBukkit;
        private System.Windows.Forms.Label labelPlayerNum;
        private System.Windows.Forms.TextBox textBoxUserNumber;
        private System.Windows.Forms.GroupBox groupBoxSuffixes;
        private System.Windows.Forms.Label labelNetherSuffix;
        private System.Windows.Forms.Label labelEndSuffix;
        private System.Windows.Forms.TextBox textBoxNetherSuffix;
        private System.Windows.Forms.TextBox textBoxEndSuffix;
        private Panel panelDontDisplayCmndBlock;
        private CheckBox checkBoxSkipCommandBlocks;
    }
}

